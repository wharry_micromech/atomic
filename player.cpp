#include <player.h>

#include <opencv2/opencv.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/highgui/highgui.hpp>


using namespace cv;
using namespace std;

Player::Player(QObject *parent)
 : QThread(parent)
{
    stop = true;
    model = (vibeModel_Sequential_t*)libvibeModel_Sequential_New();
}

bool Player::loadVideo(string filename) {
    capture  =  new cv::VideoCapture(filename);

    if (capture->isOpened())
    {
        frameRate = (int) capture->get(CV_CAP_PROP_FPS);
        if (capture->read(frame))//set the first frame as the background model
        {
            firstFrame = frame.clone();
            if (frame.channels()== 3)
                cvtColor(frame, frame, CV_BGR2GRAY);

            segmentationMap = Mat(frame.rows, frame.cols, CV_8UC1);
            libvibeModel_Sequential_AllocInit_8u_C1R(model, frame.data, frame.cols, frame.rows);
            heatMap = Mat(frame.rows, frame.cols, CV_16UC1);
            heatMap16Bit = Mat(frame.rows, frame.cols, CV_16UC1);
            falseColorsMap = Mat(frame.rows, frame.cols, CV_16UC3);
            //mergedImage = Mat(frame.rows,frame.cols,CV_8UC1);
            width = frame.cols;
            height = frame.rows;
            heat_map = (uint16_t*)(heatMap.data);
            memset(heat_map, 0, width * height);

            libvibeModel_Sequential_Segmentation_8u_C1R(model, frame.data, segmentationMap.data);
            segmentation_map = segmentationMap.data;

            emit sendFramesToDisplay(frame,segmentationMap,falseColorsMap);//segmentationMap);
        }
        return true;
    }
    else
        return false;
}

Mat Player::getActiveHeatMap()
{
    return falseColorsMap;
}

Mat Player::getActiveImage()
{
    return frame;
}

void Player::Play()
{
    if (!isRunning()) {
        if (isStopped()){
            stop = false;
        }
        start(LowPriority);
    }
}

void Player::run()
{
    int delay = (1000/frameRate);
    //double currentFrame = getCurrentFrame();
    //double lastFrame = getNumberOfFrames();
    //if(currentFrame=lastFrame)
    //    setCurrentFrame(1);

    while(!stop){
        if (capture->read(frame))
        {
            if(getCurrentFrame()==getNumberOfFrames())
                   lastFrame = frame.clone();
            if (frame.channels()== 3)
                cvtColor(frame, frame, CV_BGR2GRAY);

            /* ViBe: Segmentation and updating. */
            libvibeModel_Sequential_Segmentation_8u_C1R(model, frame.data, segmentationMap.data);
            if(resetBackgroundModel)
                libvibeModel_Sequential_Update_8u_C1R(model, frame.data, segmentationMap.data);

            /* Post-processes the segmentation map. This step is not compulsory.
                  Note that we strongly recommend to use post-processing filters, as they
                  always smooth the segmentation map. For example, the post-processing filter
                  used for the Change Detection dataset (see http://www.changedetection.net/ )
                  is a 5x5 median filter. */
            if(enableBlur)
                medianBlur(segmentationMap, segmentationMap, 3); /* 3x3 median filtering */


            for (int index = width * height - 1; index >= 0; --index) {
                if(segmentation_map[index]>0)
                {
                    uint16_t counter = heat_map[index];
                    counter+=1;
                    heat_map[index] = counter;
                }
            }

            uint32_t min=UINT_MAX;
            uint32_t max=0;

            for (int index = width * height - 1; index >= 0; --index) {
                uint32_t val = heat_map[index];
                if(val>max)
                    max = heat_map[index];
                if(val<min)
                    min = heat_map[index];
            }

            uint32_t diff = max-min;
            if(diff==0)
                diff=1;
            if(max==0)
                max=1;
            //diff = 1033;
            //cvtColor(heatMap,heatMap8Bit,CV_GRAY2BGR);

            double alpha = 65535.0/diff;
            double beta = min;
            heatMap.convertTo(heatMap16Bit,CV_16UC1,alpha,beta);
            alpha = 255.0/65535.0;
            heatMap16Bit.convertTo(heatMap8Bit,CV_8U,alpha,beta);
            applyColorMap(heatMap8Bit,falseColorsMap, cv::COLORMAP_JET);
            string frameNumber = QString::number(getCurrentFrame()).toAscii().data();
            string fileName = "heat_map_"+frameNumber+".png";
            imwrite(fileName,falseColorsMap);
            if(getCurrentFrame()==getNumberOfFrames())
            {
                imwrite("lastHeatMap.PNG",heatMap);
                imwrite("lastHeatMap16Bit.PNG",heatMap16Bit);
                imwrite("lastHeatMap8Bit.PNG",heatMap8Bit);
                imwrite("lastHeatMapFalseColor.PNG",falseColorsMap);
                addWeighted(firstFrame,0.8,falseColorsMap,0.2,0.0,mergedImage);
                imwrite("firstFrame.PNG",firstFrame);
                imwrite("mergedImage.PNG",mergedImage);
                imwrite("lastFrame.PNG",lastFrame);

            }

            emit sendFramesToDisplay(frame,segmentationMap,falseColorsMap);
        }
        else
        {
            /* Delete capture object. */
            capture->release();
            /* Frees the model. */
            libvibeModel_Sequential_Free(model);
            Stop();
        }

        this->msleep(delay);
    }
}


Player::~Player()
{
    mutex.lock();
    stop = true;
    capture->release();
    delete capture;
    condition.wakeOne();
    mutex.unlock();
    wait();
}

void Player::Stop()
{
    stop = true;
}

void Player::msleep(int ms){
    struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
    nanosleep(&ts, NULL);
}

bool Player::isStopped() const{
    return this->stop;
}

double Player::getCurrentFrame(){

    return capture->get(CV_CAP_PROP_POS_FRAMES);
}

double Player::getNumberOfFrames(){

    return capture->get(CV_CAP_PROP_FRAME_COUNT);
}

double Player::getFrameRate(){
    return frameRate;
}

void Player::setCurrentFrame( int frameNumber )
{
    capture->set(CV_CAP_PROP_POS_FRAMES, frameNumber);
}

int Player::getNumberOfSamples()
{
    return libvibeModel_Sequential_GetNumberOfSamples(model);
}

int Player::getMatchingNumber()
{
    return libvibeModel_Sequential_GetMatchingNumber(model);
}

int Player::getMatchingThreshold()
{
    return libvibeModel_Sequential_GetMatchingThreshold(model);
}

int Player::getUpdateFactor()
{
     return libvibeModel_Sequential_GetUpdateFactor(model);
}

void Player::setMatchingNumber(int matchingNumber)
{
    libvibeModel_Sequential_SetMatchingNumber(model,matchingNumber);
}

void Player::setMatchingThreshold(int matchingThreshold)
{
    libvibeModel_Sequential_SetMatchingThreshold(model,matchingThreshold);
}

void Player::setUpdateFactor(int updateFactor)
{
     libvibeModel_Sequential_SetUpdateFactor(model,updateFactor);
}

Mat Player::getMergedImage()
{
    return mergedImage;
}
