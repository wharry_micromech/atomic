#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QFileDialog>
#include <QSpinBox>
#include <QMessageBox>
#include <QMainWindow>
#include <player.h>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    //Display video frame in player UI
    void updatePlayerUI(const Mat& img,const Mat& segmentedImg,const Mat& heatMapImg);
    void on_pushButton_clicked();
    void on_processVideoButton_clicked();
    QString getFormattedTime(int timeInSeconds);
    void on_horizontalSlider_sliderPressed();
    void on_horizontalSlider_sliderReleased();
    void on_horizontalSlider_sliderMoved(int position);

    void on_spinBox_valueChanged(int arg1);

    void on_spinBox_MatchingNumber_editingFinished();

    void on_spinBox_editingFinished();

    void on_spinBox_UpdateFactor_editingFinished();

    void on_checkBox_clicked(bool checked);

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_checkBox_ResetBackground_clicked(bool checked);

private:
    Ui::MainWindow *ui;
    Player* myPlayer;
};

#endif // MAINWINDOW_H
