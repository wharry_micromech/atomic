#ifndef PLAYER_H
#define PLAYER_H
#include <QMutex>
#include <QThread>
#include <QImage>
#include <QWaitCondition>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "vibe-background-sequential.h"
using namespace cv;
using namespace std;
class Player : public QThread
{    Q_OBJECT
 private:
    bool stop;
    QMutex mutex;
    QWaitCondition condition;
    Mat frame;
    Mat firstFrame;
    Mat lastFrame;
    int frameRate;
    VideoCapture *capture;
    Mat RGBframe;
    QImage img;
    Mat segmentationMap;        /* Will contain the segmentation map. This is the binary output map. */
    Mat heatMap;
    Mat heatMap16Bit;
    Mat heatMap8Bit;
    Mat falseColorsMap;
    Mat mergedImage;

    uint16_t *heat_map;
    uint32_t width;
    uint32_t height;
    uint8_t *segmentation_map;
    /* Model for ViBe. */
    vibeModel_Sequential_t *model = NULL; /* Model used by ViBe. */
 signals:
 //Signal to output frame to be displayed
      void sendFramesToDisplay(const Mat &frame,const Mat &segmentedImg,const Mat &heatMapImg);
 protected:
     void run();
     void msleep(int ms);
 public:
    //Constructor
    Player(QObject *parent = 0);
    //Destructor
    ~Player();
    //Load a video from memory
    bool loadVideo(string filename);
    //Play the video
    void Play();
    //Stop the video
    void Stop();
    //check if the player has been stopped
    bool isStopped() const;

    //set video properties
    void setCurrentFrame( int frameNumber);
    //Get video properties
    double getFrameRate();
    double getCurrentFrame();
    double getNumberOfFrames();
    int getNumberOfSamples();
    int getMatchingNumber();
    int getMatchingThreshold();
    int getUpdateFactor();
    void setMatchingNumber(int matchingNumber);
    void setMatchingThreshold(int matchingThreshold);
    void setUpdateFactor(int updateFactor);
    Mat getActiveHeatMap();
    Mat getActiveImage();
    Mat getMergedImage();
    bool enableBlur=true;
    bool resetBackgroundModel=false;

};
#endif // VIDEOPLAYER_H
