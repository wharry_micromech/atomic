#-------------------------------------------------
#
# Project created by QtCreator 2016-04-18T11:59:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AtomicViBe
TEMPLATE = app

INCLUDEPATH += /usr/local/include/opencv
LIBS += -L/usr/local/lib -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_contrib
QMAKE_CFLAGS += -std=c99
SOURCES += main.cpp\
        mainwindow.cpp \
        vibe-background-sequential.c \
    player.cpp


HEADERS  += mainwindow.h \
    vibe-background-sequential.h \
    player.h

FORMS    += mainwindow.ui
QT += widgets
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x000000
