# README #



### What is this repository for? ###

* Application for detecting changes in atomic structure during pico indendation
* Version 1.0

#    INSTRUCTIONS FOR BUILDING AND RUNNING AtomicVibe   #
#STEP 1: Clone the AtomicVibe repository into your local code directory
```
#!bash
git clone https://amburan@bitbucket.org/amburan/atomicvibe.git
git checkout master
```

#STEP 2: Install QTCreator (tested on ubuntu 14.04 and 16.04) and QT 5
```
#!bash
sudo apt-get install qtcreator
sudo apt-get install qt5-default
```

#STEP 3: Install OpenCV (tested on ubuntu 16.04)
```
#!bash
sudo apt-get install libopencv-dev
```

#STEP 4: Open the project (AtomicViBe.pro in the folder atomicvibe) in QtCreator and select "Run" from "Build" menu. This should load the test video in the folder and lauch the UI. Click the "Play" button to see the three windows: the input video, vibe segmented video and the heat map video.

### Who do I talk to if I run into trouble? ###

* stephenthomas1@boisestate.edu