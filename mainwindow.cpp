#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <opencv2/opencv.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/highgui/highgui.hpp>


#include "vibe-background-sequential.h"

using namespace cv;
using namespace std;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    qRegisterMetaType< Mat >("Mat");
     myPlayer = new Player();
    QObject::connect(myPlayer, SIGNAL(sendFramesToDisplay(const Mat&, const Mat&, const Mat&)),
                                 this, SLOT(updatePlayerUI(const Mat&, const Mat&, const Mat&)));
    ui->setupUi(this);
    ui->processVideoButton->setEnabled(false);
    ui->horizontalSlider->setEnabled(false);
    /* Create GUI windows. */
    namedWindow("Frame", WINDOW_NORMAL);
    namedWindow("Segmentation by ViBe", WINDOW_NORMAL);
    namedWindow("Heatmap by AtomicViBe",WINDOW_NORMAL);
    ui->spinBox->setValue(myPlayer->getMatchingThreshold());
    ui->spinBox_MatchingNumber->setValue(myPlayer->getMatchingNumber());
    ui->spinBox_UpdateFactor->setValue(myPlayer->getUpdateFactor());
    ui->checkBox->setChecked(myPlayer->enableBlur);
    ui->checkBox->setChecked(myPlayer->resetBackgroundModel);

    //string filename = "../window 3_location 2_0013 DC_compressed_gs.avi";
    //string filename = "../ODS_Fe-Irr_3dpa_500C_Window2__0001 DC_compressed.avi";
    //string filename = "../ODSFe3Window3__0001 DC_compressed.avi";
    //string filename = "../3ptbend_0000 DC_compressed.avi";
    //string filename = "../opt2beam3_0002 DC_compressed.avi";
    //string filename = "../Fe Irr ODS 3dpa 500C_3-17_600x100_Pillar_0000 DC_compression.avi";
    string filename = "window1_0000DC.avi";

    if(myPlayer->loadVideo(filename))
    {
        this->setWindowTitle("ODS_Fe-Irr_3dpa_500C_Window2__0001 DC_compressed.avi");
        ui->processVideoButton->setEnabled(true);
        ui->horizontalSlider->setEnabled(true);
        ui->horizontalSlider->setMaximum(myPlayer->getNumberOfFrames());
        int nFrames = (int)myPlayer->getNumberOfFrames();
        ui->label_3->setText( getFormattedTime( nFrames/(int)myPlayer->getFrameRate()) );
        ui->label_framerate->setText(QString::number(myPlayer->getFrameRate()));
    }
    else
    {
        this->setWindowTitle("No videos loaded!");
    }
}

void MainWindow::updatePlayerUI(const Mat& originalImg,const Mat& segmentedImg,const Mat& colorMapImg)
{
    imshow("Frame",originalImg);
    imshow("Segmentation by ViBe",segmentedImg);
    imshow("Heatmap by AtomicViBe",colorMapImg);
    ui->horizontalSlider->setValue(myPlayer->getCurrentFrame());
    ui->label_2->setText( getFormattedTime( (int)myPlayer->getCurrentFrame()/(int)myPlayer->getFrameRate()) );
}

MainWindow::~MainWindow()
{
    delete myPlayer;
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
   QString filename = QFileDialog::getOpenFileName(this,
                                             tr("Open Video"), ".",
                                             tr("Video Files (*.avi *.mpg *.mp4)"));
   QFileInfo name = filename;

   if (!filename.isEmpty()){
       if (!myPlayer->loadVideo(filename.toAscii().data()))
       {
           QMessageBox msgBox;
           msgBox.setText("The selected video could not be opened!");
           msgBox.exec();
       }
       else
       {
           this->setWindowTitle(name.fileName());
           ui->processVideoButton->setEnabled(true);
           ui->horizontalSlider->setEnabled(true);
           ui->horizontalSlider->setMaximum(myPlayer->getNumberOfFrames());
           ui->label_3->setText( getFormattedTime( (int)myPlayer->getNumberOfFrames()/(int)myPlayer->getFrameRate()) );
       }
   }
    /*QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"/home/sthomas/Downloads",tr("AVI Files (*.avi)"));
    ui->fileNameLabel->setText(fileName);
    ui->processVideoButton->setEnabled(true);*/
}

void MainWindow::on_processVideoButton_clicked()
{
   if (myPlayer->isStopped())
   {
       myPlayer->Play();
       ui->processVideoButton->setText(tr("Pause/Stop"));
   }else
   {
       myPlayer->Stop();
       ui->processVideoButton->setText(tr("Play"));
   }
   /* QByteArray ba = ui->fileNameLabel->text().toLatin1();
    char *c_str2 = ba.data();
    processVideo(c_str2);*/
}

QString MainWindow::getFormattedTime(int timeInSeconds){

    int seconds = (int) (timeInSeconds) % 60 ;
    int minutes = (int) ((timeInSeconds / 60) % 60);
    int hours   = (int) ((timeInSeconds / (60*60)) % 24);

    QTime t(hours, minutes, seconds);
    if (hours == 0 )
        return t.toString("mm:ss");
    else
        return t.toString("h:mm:ss");
}

void MainWindow::on_horizontalSlider_sliderPressed()
{
    myPlayer->Stop();
}

void MainWindow::on_horizontalSlider_sliderReleased()
{
    myPlayer->Play();
}

void MainWindow::on_horizontalSlider_sliderMoved(int position)
{
    myPlayer->setCurrentFrame(position);
    ui->label_2->setText( getFormattedTime( position/(int)myPlayer->getFrameRate()) );
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{

}

void MainWindow::on_spinBox_MatchingNumber_editingFinished()
{
     myPlayer->setMatchingNumber(ui->spinBox_MatchingNumber->value());
}

void MainWindow::on_spinBox_editingFinished()
{
    myPlayer->setMatchingThreshold(ui->spinBox->value());
}

void MainWindow::on_spinBox_UpdateFactor_editingFinished()
{
    myPlayer->setUpdateFactor(ui->spinBox_UpdateFactor->value());
}

void MainWindow::on_checkBox_clicked(bool checked)
{
    myPlayer->enableBlur = checked;
}

void MainWindow::on_pushButton_2_clicked()
{
    myPlayer->Stop();
    QString filename = QFileDialog::getSaveFileName(this,
                                              tr("Save Heatmap"), ".",
                                              tr("Image Files (*.jpg *.png *.bmp)"));
    //QFileInfo name = filename;

    if (!filename.isEmpty()){
         imwrite(filename.toAscii().data(),myPlayer->getActiveHeatMap());
    }

}

void MainWindow::on_pushButton_3_clicked()
{
    myPlayer->Stop();
    QString filename = QFileDialog::getSaveFileName(this,
                                              tr("Save Image"), ".",
                                              tr("Image Files (*.jpg *.png *.bmp)"));
    //QFileInfo name = filename;

    if (!filename.isEmpty()){
         imwrite(filename.toAscii().data(),myPlayer->getActiveImage());
    }

}

void MainWindow::on_checkBox_ResetBackground_clicked(bool checked)
{
    myPlayer->resetBackgroundModel = checked;
}
